package de.bcmsolutions.restservice.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import de.bcmsolutions.restservice.dao.interfaces.ProductDao;
import de.bcmsolutions.restservice.entity.Product;
import de.bcmsolutions.teamtool.entity.Role;

@Repository
public class ProductDaoImpl implements ProductDao
{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void saveProduct(Product product)
	{		
		Session session = this.entityManager.unwrap(Session.class);
		
		session.saveOrUpdate(product);
	}

	@Override
	public Product getProduct(int id)
	{
		Session session = this.entityManager.unwrap(Session.class);
		
		Product product = null;
		
		product = session.get(Product.class, id);
		
		return product;
	}

	@Override
	public List<Product> getProducts()
	{
		Session session = this.entityManager.unwrap(Session.class);
		
		Query<Product> query = session.createQuery("from Product", Product.class);
		
		List<Product> products = query.getResultList();
		
		return products;
	}

	@Override
	public void deleteProduct(int id)
	{
		Session session = this.entityManager.unwrap(Session.class);
		
		Query<Product> query = session.createQuery("delete from Product where id = \" + id", Product.class);
		
		query.executeUpdate();	
	}
}
