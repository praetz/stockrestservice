package de.bcmsolutions.restservice.dao.interfaces;

import java.util.List;

import de.bcmsolutions.restservice.entity.Product;

public interface ProductDao
{	
	void saveProduct(Product product);
		
	Product getProduct(int id);
		
	List<Product> getProducts();
		
	void deleteProduct(int id);
}