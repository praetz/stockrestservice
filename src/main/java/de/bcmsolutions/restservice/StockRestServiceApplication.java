package de.bcmsolutions.restservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockRestServiceApplication 
{
	public static void main(String[] args) {
		SpringApplication.run(StockRestServiceApplication.class, args);
	}

}
