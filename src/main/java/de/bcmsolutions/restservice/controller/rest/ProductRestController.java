package de.bcmsolutions.restservice.controller.rest;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.bcmsolutions.restservice.entity.Product;
import de.bcmsolutions.restservice.service.interfaces.ProductService;


@RestController
@RequestMapping("/api/products")
public class ProductRestController
{
	private final String productNotFound = "Product not found";
	
	@Autowired
	private ProductService productService;		
		
	private List<Product> products;
		
	@PostConstruct
	private void initialize()
	{	
		this.products = this.productService.getProducts();
	}
		
	@GetMapping("/")
	public List<Product> getProducts()
	{	
		return products;
	}

	@GetMapping("/{productId}")
	public String getProductById(@PathVariable int productId)
	{	
		Product product = this.findProductById(productId);
		
		return  product == null ? this.productNotFound : product.toString();		
	}
	
	@GetMapping("/{productId}/stock")
	public String getProductStockById(@PathVariable int productId)
	{
		Product product = this.findProductById(productId);
		
		return  product == null ? this.productNotFound : Integer.toString(product.getStock());
	}
	
	/*
	 * Updates the stock of a product (if it exists) by a Product-Object sent as a JSON-Object.
	 * The number in the stock-field will be added to determine the new stock/inventory if possible.  
	 * */
	@PutMapping("/{productId}")
	public String updateStock(@RequestBody Product updateProduct)
	{
		Product product = this.findProductById(updateProduct.getId());
		
		if(product == null)
		{
			return this.productNotFound;
		}
		else
		{
			if(product.getStock() + updateProduct.getStock() < 0)
			{
				return "Stock change not possible";
			}
			else
			{
				product.setStock(product.getStock() + updateProduct.getStock());
				this.productService.saveProduct(product);
				return product.toString();
			}
		}		
	}
		
	private Product findProductById(int productId)
	{
		return (this.products.parallelStream().filter(
				product -> product.getId() == productId).findFirst()).orElse(null);
	}
}
