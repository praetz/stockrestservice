package de.bcmsolutions.restservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.bcmsolutions.restservice.dao.interfaces.ProductDao;
import de.bcmsolutions.restservice.entity.Product;
import de.bcmsolutions.restservice.service.interfaces.ProductService;


@Service
public class ProductServiceImpl implements ProductService
{	
	@Autowired
	private ProductDao productDao;

	@Override
	@Transactional
	public void saveProduct(Product product)
	{
		this.productDao.saveProduct(product);
	}

	@Override
	@Transactional
	public Product getProduct(int id)
	{
		return this.productDao.getProduct(id);
	}

	@Override
	@Transactional
	public List<Product> getProducts()
	{
		return this.productDao.getProducts();
	}

	@Override
	@Transactional
	public void deleteProduct(int id)
	{
		this.productDao.deleteProduct(id);
	}
}
