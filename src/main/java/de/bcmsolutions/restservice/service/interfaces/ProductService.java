package de.bcmsolutions.restservice.service.interfaces;

import java.util.List;

import org.springframework.stereotype.Service;

import de.bcmsolutions.restservice.entity.Product;

@Service
public interface ProductService
{
	void saveProduct(Product product);
	
	Product getProduct(int id);
		
	List<Product> getProducts();
		
	void deleteProduct(int id);	

}
